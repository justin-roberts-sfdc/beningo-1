import BoardContainer from './containers/BoardContainer';
import BoardLayout from '../../layouts/BoardLayout/BoardLayout';
import boardReducer from './modules/board';
import { injectReducer } from '../../store/reducers';

// Sync route definition
export default (store) => {
    injectReducer(store, {key: 'board', reducer: boardReducer});
    return {
        component: BoardLayout,
        indexRoute:
        {
            component: BoardContainer,
        },
        childRoutes: []
    };
};
