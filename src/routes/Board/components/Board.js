import React from 'react';
import PropTypes from 'prop-types';
import { Component } from 'react';
import { renderSpinner } from '../../../lib/UiUtils';
import { log } from "../../../lib/Logger";
import {
} from '../../../lib/Constants';
import Confetti from 'react-confetti';
import { getCenterCoordinates } from '../../../lib/GeneralUtils';
import {getBoardType} from "../../../lib/Constants";
import {getCenterImageName} from "../../../lib/Constants";

class Board extends Component {
    constructor(props) {
        super(props);
        this.state = {
            type: getBoardType(props.location.query),
        };
        this.handleCellClick = this.handleCellClick.bind(this);
    }

    componentDidMount() {
        let {
            wordPool,
            init,
        } = this.props;
        if (wordPool.length === 0) {
            log(`Word pool empty, initializing...`);
            init(this.state.type);
        }
        console.log(`Params: ${JSON.stringify(this.props.location.query)}`);
    }

    handleCellClick(event) {
        let {
            toggleSquare,
            boardSize: {
                x: boardCellsX,
                y: boardCellsY,
            },
        } = this.props;
        let attributes = event.currentTarget.attributes;
        let x = parseInt(attributes['data-x'].value, 10);
        let y = parseInt(attributes['data-y'].value, 10);
        let [centerX, centerY] = getCenterCoordinates(boardCellsX, boardCellsY);
        // Ignore clicks on the free space
        if (x === centerX && y === centerY) {
            return;
        }
        console.log(`Clicked x:${x}, y:${y}`);
        toggleSquare(x, y);
    }

    render() {
        let {
            selectedCells,
            cellWords,
            boardSize: {
                x: boardCellsX,
                y: boardCellsY,
            },
            wordPool,
            bingos,
            hasBingoed,
        } = this.props;

        if (wordPool.length === 0) {
            return <div>
                {renderSpinner('fa-5x')}
            </div>;
        }

        let center = getCenterCoordinates(boardCellsX, boardCellsY);

        return <div className="full-size">
            {this.renderConfetti(hasBingoed, this.props.size)}
            <div className="grid-container full-size" style={{
                gridTemplateColumns: `repeat(${boardCellsX}, 1fr)`,
                gridTemplateRows: `repeat(${boardCellsY}, 1fr)`,
            }}>
                {[...Array(boardCellsY)].map((_, y) => {
                    return [...Array(boardCellsX)].map((_, x) => {
                        return <div key={`cell-container-${x}-${y}`}
                                    className={`cell-container ` +
                                        `${bingos[y][x] ? 'bingoed ' : ' '}` +
                                        `${y === 0 ? 'top-row ' : ' '}` +
                                        `${y === boardCellsY - 1 ? 'bottom-row ' : ' '}` +
                                        `${x === 0 ? 'first-column ' : ' '}` +
                                        `${x === boardCellsX - 1 ? 'last-column ' : ' '}`}
                                    onClick={this.handleCellClick} style={{
                            gridColumnStart: x,
                            gridRowStart: y,
                        }} data-x={x} data-y={y}>
                            {this.renderCellContent(x, y, cellWords, selectedCells[y][x], center)}
                        </div>;
                    });
                })}
            </div>
        </div>;
    }

    renderCellContent(x, y, cellWords, isSelected, center) {
        let [centerX, centerY] = center;
        if (x === centerX && y === centerY) {
            // return <div className="free-space-container">
            return <img className="free-space" src={getCenterImageName(this.state.type)} />;
        } else if (isSelected) {
            return <div className="selected-cell">
                {this.renderCellText(x, y, cellWords)}
            </div>;
        } else {
            return this.renderCellText(x, y, cellWords);
        }
    }

    renderCellText(x, y, cellWords) {
        return <p key={`cell-text-${x}-${y}`} className="cell-text">{cellWords[y][x]}</p>;
    }

    renderConfetti(hasBingoed, size) {
        if (!hasBingoed) {
            return null;
        }
        return <Confetti {...size} />;
    }
}

Board.propTypes = {
    // inputCategory: PropTypes.func.isRequired,
    // selectedCategory: PropTypes.number.isRequired,
};

export default Board;
