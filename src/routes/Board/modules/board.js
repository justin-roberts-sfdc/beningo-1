import { log } from "../../../lib/Logger";
import WordPool from "../../../lib/WordPool";
import { getCenterCoordinates } from '../../../lib/GeneralUtils';

// ------------------------------------
// Constants
// ------------------------------------
export const INIT = 'INIT';

export const TOGGLE_SQUARE = 'TOGGLE_SQUARE';
export const DESELECT_SQUARE = 'DESELECT_SQUARE';

export const SET_BOARD_SIZE = 'SET_BOARD_SIZE';
export const RELOAD_SQUARES = 'RELOAD_SQUARES';

// ------------------------------------
// Actions
// ------------------------------------
export const toggleSquare = (x, y) => {
    return {
        type: TOGGLE_SQUARE,
        x,
        y
    };
};

export const init = (boardType) => {
    return {
        type: INIT,
        boardType,
    };
};

// ------------------------------------
// Thunks
// ------------------------------------

// ------------------------------------
// Reducer
// ------------------------------------
const initialState = {
    // rows [columns]
    selectedCells: [],
    cellWords: [],
    boardSize: {
        x: 5,
        y: 5
    },
    wordPool: [],
};
export default function boardReducer(state = initialState, action) {
    switch (action.type) {
        case INIT:
            let boardType = action.boardType;
            let words = WordPool[boardType];
            return {
                ...state,
                wordPool: words,
                selectedCells: clearSelectedCells(state.boardSize.x, state.boardSize.y),
                cellWords: distributeWords(state.boardSize.x, state.boardSize.y, words),
            };
        case TOGGLE_SQUARE:
            let existingValue = state.selectedCells[action.y][action.x];
            return {
                ...state,
                selectedCells: immutableUpdateObjectInArray(state.selectedCells, action.y,
                    immutableUpdateObjectInArray(state.selectedCells[action.y], action.x, !existingValue)),
            };
        default:
            return state;
    }
}

function clearSelectedCells(xCells, yCells) {
    let cells = [...Array(yCells)].map((y) => {
        return [...Array(xCells)].map((x) => {
            return false;
        });
    });
    let [centerX, centerY] = getCenterCoordinates(xCells, yCells);
    cells[centerY][centerX] = true;
    return cells;
}

function distributeWords(xCells, yCells, wordPool) {
    let words = wordPool.slice(0);
    return [...Array(yCells)].map((y) => {
        return [...Array(xCells)].map((x) => {
            let wordIndex = Math.round(Math.random() * (words.length - 1));
            return words.splice(wordIndex, 1);
        });
    });
}

function immutableUpdateObjectInArray(array, index, newValue) {
    return array.map((value, i) => {
        if(i !== index) {
            // This isn't the droid we are looking for
            return value;
        }

        // Otherwise, this is the one we want - return an updated value
        return newValue;
    });
}
