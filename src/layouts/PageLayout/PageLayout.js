import React from 'react';
import { connect } from 'react-redux';
import { IndexLink, Link } from 'react-router';
import Modal from 'react-modal';
import PropTypes from 'prop-types';
import { Component } from 'react';
import {
    dismissModal,
    MODAL_TYPE_ERROR,
    MODAL_TYPE_INFO,
    MODAL_TYPE_WARNING,
    MODAL_TYPE_SUCCESS,
} from '../../store/globalUi';
import './Global.scss';

class PageLayout extends Component {
    render() {
        let {
            children,
            dismissModal,
            globalUi: {
                modalMessage,
                modalMessageType,
            }
        } = this.props;

        return (
            <div className="h-100">
                <link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet"/>
                <div className='h-100 w-100 text-center'>
                    <p className="author-credit">By Dan van Kley. Dubious assistance by Rob Righter and "Eric" Berg.</p>
                    {this.renderModal(modalMessage, modalMessageType, dismissModal)}
                        {children}
                </div>
            </div>
        );
    }

    renderModal(modalMessage, modalMessageType, dismissModal) {
        let alertClassType;
        switch (modalMessageType) {
            case MODAL_TYPE_ERROR:
                alertClassType = 'alert-danger';
                break;
            case MODAL_TYPE_WARNING:
                alertClassType = 'alert-warning';
                break;
            case MODAL_TYPE_INFO:
                alertClassType = 'alert-info';
                break;
            case MODAL_TYPE_SUCCESS:
                alertClassType = 'alert-info';
                break;
        }
        // onAfterOpen={this.afterOpenModal}
        // onRequestClose={this.closeModal}
        // style={customStyles}
        return (
            <div>
                <Modal isOpen={Boolean(modalMessage)} contentLabel={`Alert: ${modalMessage}`} >
                    <div className={`alert ${alertClassType}`} role="alert">
                        {modalMessage}
                    </div>
                    <button onClick={dismissModal} type="button" className="btn btn-primary">Ok</button>
                </Modal>
            </div>
        );
    }
}

PageLayout.propTypes = {
    children: PropTypes.node,
    // modalMessage: PropTypes.string,
};

const mapStateToProps = (state) => ({
    globalUi: state.globalUi,
});

const mapDispatchToProps = {
    dismissModal,
};

export default connect(mapStateToProps, mapDispatchToProps)(PageLayout);
