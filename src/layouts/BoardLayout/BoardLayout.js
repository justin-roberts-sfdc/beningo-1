import React from 'react';
import { connect } from 'react-redux';
import PropTypes from 'prop-types';
import { Component } from 'react';
import './BoardLayout.scss';

class BoardLayout extends Component {
    render() {
        let {
            children,
            location: {
                pathname
            },
        } = this.props;

        return <div className='board-container'>
            <div className='header-container'>
                <p className="header">BENINGO</p>
            </div>
            {children}
        </div>;
    }
}

BoardLayout.propTypes = {
    children: PropTypes.node,
};

const mapStateToProps = (state) => ({
});

const mapDispatchToProps = {
};

export default connect(mapStateToProps, mapDispatchToProps)(BoardLayout);
