import { logLevel } from './Constants';
export const NONE = 'NONE';
export const ERROR = 'ERROR';
export const WARN = 'WARN';
export const INFO = 'INFO';
export const TRACE = 'TRACE';

const heirarchy = [TRACE, INFO, WARN, ERROR, NONE];

/**
 * @param message {string}
 * @param level {string}
 */
export function log(message, level = INFO) {
    if (heirarchy.indexOf(level) >= heirarchy.indexOf(logLevel)) {
        console.log(`${level}: ${message}`);
    }
}