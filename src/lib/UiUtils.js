import React from 'react';
import {
    displayModal,
    MODAL_TYPE_ERROR
} from "../store/globalUi";

export function renderErrorFlashMessage(errorMessage) {
    if (errorMessage) {
        return (
            <div className="alert alert-danger" role="alert">
                <strong>Whoops!</strong> {errorMessage}
            </div>
        );
    }
    return '';
}

export function renderSpinner(size = 'fa-lg') {
    return (
        <i className={`fa fa-spinner fa-spin ${size}`}/>
    );
}

export const modalErrorHandler = function(dispatch, actionType, message) {
    dispatch({type: actionType});
    dispatch(displayModal(message, MODAL_TYPE_ERROR));
};
